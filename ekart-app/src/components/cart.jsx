import React, { Component } from "react";
import Product from "./product";
import TotalCounter from "./totalCounter";
class Cart extends Component {
  state = {
    products: [
      { id: 1, name: "pen", quantity: 4, price: 20 },
      { id: 2, name: "pencil", quantity: 2, price: 5 },
      { id: 3, name: "notebook", quantity: 5, price: 40 },
      { id: 4, name: "eraser", quantity: 2, price: 10 },
    ],
  };
  render() {
    return (
      <div>
        {this.state.products.map((product) => (
          <Product
            key={product.id}
            value={product}
            onIncrement={this.handleIncrement}
            onDecrement={this.handleDecrement}
          />
        ))}

        <TotalCounter value={this.state.products} />
      </div>
    );
  }

  handleDecrement = (id) => {
    console.log("decrement called:" + id);
    const products = [...this.state.products];
    const productToBeUpdated = this.state.products.filter(
      (product) => product.id === id
    )[0];
    const productIndex = products.indexOf(productToBeUpdated);
    productToBeUpdated.quantity--;

    products[productIndex] = productToBeUpdated;

    this.setState({ products });
  };

  handleIncrement = (id) => {
    console.log("increment called:" + id);
    const products = [...this.state.products];
    const productToBeUpdated = this.state.products.filter(
      (product) => product.id === id
    )[0];
    const productIndex = products.indexOf(productToBeUpdated);
    productToBeUpdated.quantity++;

    products[productIndex] = productToBeUpdated;

    this.setState({ products });
  };
}

export default Cart;
