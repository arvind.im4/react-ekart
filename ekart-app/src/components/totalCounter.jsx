import React, { Component } from "react";

class TotalCounter extends Component {
  render() {
    const { value } = this.props;
    return <h4> total:{this.calculateTotalPrice(value)}</h4>;
  }

  calculateTotalPrice = (products) => {
    let totalValue = 0;

    for (let i = 0; i < products.length; i++) {
      let product = products[i];
      totalValue = totalValue + product.price * product.quantity;
    }

    return totalValue;
  };
}

export default TotalCounter;
