import React, { Component } from "react";
import Counter from "./counter";

class Product extends Component {
  render() {
    const { value } = this.props;
    return (
      <div>
        <h4>
          {value.name} ~ Rs: {value.price}
        </h4>
        {this.renderCounter(value)}
        <hr />
      </div>
    );
  }

  renderCounter = (value) => {
    return value.quantity === 0 ? (
      <Counter onChange={() => this.props.onIncrement(value.id)} value="Add" />
    ) : (
      <React.Fragment>
        <Counter onChange={() => this.props.onDecrement(value.id)} value="-" />
        {value.quantity}{" "}
        <Counter onChange={() => this.props.onIncrement(value.id)} value="+" />
      </React.Fragment>
    );
  };
}

export default Product;
