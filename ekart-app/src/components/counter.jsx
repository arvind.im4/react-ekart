import React, { Component } from "react";

class Counter extends Component {
  render() {
    return (
      <div style={{ display: "inline" }}>
        <button onClick={this.props.onChange}>{this.props.value} </button>
      </div>
    );
  }
}

export default Counter;
